# Description
Python package used to conduct ETL & Machine learning on dataset


# Installation on a docker machine running anaconda

Create a python 2.7 env if one doesnt already exist

Get the package source code (should be on the master branch)
**cd /tmp**
**git clone https://oliver-gauss-io@bitbucket.org/gaussanalytics/spark-etl-iri.git**


**cd spark-etl-iri/promo_analysis_etl/GaussAnalyzer**
Make the shell file executeable
**chmod +x pip_installer.sh**
Run the pip installer to build the package and install it in the lpcal python env
**./pip_installer.sh**

optional (clean up the source code and package file)
**cd /tmp**
**rm -rf spark-etl-iri**

open a python shell
**python**

## ----- Inside python shell ----
**import GaussAnalyzer as ga**
run the PromoAnalyzer use case for a given item, vend pair
**ga.doPromoAnalysisOnIriDataset(item='41351',vend='37000')**

# References
[https://python-packaging.readthedocs.io/en/latest/minimal.html for packaging info]

