import imp
import os
from setuptools import setup, find_packages

def readme():
    with open('README.rst') as f:
        return f.read()

version = imp.load_source(
    'version', os.path.join('GaussAnalyzer', 'version.py'))


setup(name='GaussAnalyzer',
      version='0.0.1',
      description='Python scripts used to analyze data',
      long_description=readme(),
      classifiers=[
        'Development Status :: 3 - Alpha',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 2.7',
        'Topic :: Retail Data Analytics :: Machine Learning',
      ],
      url='https://bitbucket.org/gaussanalytics/spark-etl-iri/src',
      author='Oliver Saleh',
      author_email='oliver@gauss.ai',
      license='MIT',
      packages=find_packages(),
      install_requires=[
          'pandas',
          'psycopg2',
          'SQLAlchemy'
      ],
      include_package_data=True,
      zip_safe=False)