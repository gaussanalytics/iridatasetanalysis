# -*- coding: utf-8 -*-
"""
Created on Thu Jul 21 15:46:03 2016

@author: oliversaleh
""" 
import pandas as pd
#import numpy as np
#import os
import psycopg2
#import mysql.connector
from sqlalchemy import create_engine, types#, MetaData
import hashlib

# sample useage:
# pr = PromoAnalyzer()
# pr.performEtl('41351','37000')

class PromoAnalyzer():

    def __init__(self):
        #self.file_path= file_path
        # Todo: secure this more
        # jdbc:redshift://iri-dataset.chz8mzfebvxp.us-west-2.redshift.amazonaws.com:5439/iridataset
        self.__HOST = 'iri-dataset.chz8mzfebvxp.us-west-2.redshift.amazonaws.com'
        self.__PORT = 5439 # redshift default
        self.__USER = 'oliver'
        self.__PASSWORD = 'qnTT5+9*45eeW!jodP'
        self.__DATABASE = 'iridataset'
        print("Initialized a PromoAnalyzer instance")

    def generateMD5Hash(self, inputString):
        # Assumes the default UTF-8
        return hashlib.md5(inputString.replace(" ", "").encode()).hexdigest()
             
                
    def redshift_db_connection(self):
        conn = psycopg2.connect(
            host=self.__HOST,
            port=self.__PORT,
            user=self.__USER,
            password=self.__PASSWORD,
            database=self.__DATABASE,
        )
        print("Established connection to Redshift")
        return conn

    def runQueryOnRedshift(self,item=None,vend=None):
        # assuming year 12
        iri_item=item
        iri_vend=vend
        sqlQuerypart1 = "SELECT item, vend, iri_key, week, sy, ge, units, dollars, f, d, pr FROM years.retailers_year12 "
        
        # no need to sort as we will sort locally later
        #sqlQuerypart2 = " order by iri_key ,week ASC;"

        optionalWhereClause = ''
        if iri_item != None:
            optionalWhereClause += 'item = ' + iri_item + ' '
        if iri_vend != None:
            if len(optionalWhereClause) > 0:
                optionalWhereClause += 'AND '
                
            optionalWhereClause += 'vend = ' + iri_vend + ' '
        
        if len(optionalWhereClause) > 0:
            # we have either item or vend or both specified
            optionalWhereClause = 'WHERE ' +  optionalWhereClause #+ ' LIMIT 100;'
        else:
            # we dont have either item or vend specified
            optionalWhereClause = ';'
        
        sqlQuery = sqlQuerypart1 + optionalWhereClause
        print sqlQuery

        # conn = db_connection()
        #try:
        #    cursor = conn.cursor()
        #    cursor.execute(sqlQuery)
        #    df=pd.DataFrame(cursor.fetchall(),columns=
        #    ['item', 'vend', 'iri_key', 'week', 'sy', 'ge', 'units', 'dollars', 'f', 'd', 'pr'])
        #
        #    #results = cursor.fetchall() # careful, the results could be huge
        #    conn.commit()
        #    print results
        #finally:
        #    conn.close()

        # using pandas
        conn = self.redshift_db_connection()
        df = pd.DataFrame()
        try:
            print("Running Query on Redshift")
            df = pd.read_sql(sqlQuery, conn)
            #df.to_csv('tmp/queryresult.csv', index=False)
        finally:
            print("Closing connection to Redshift")
            conn.close()
            
        return df

    def performEtl(self, item=None,vend=None):
        
        # from progressbar import Bar
        from collections import OrderedDict

        iri_year=12

        first_week =0
        last_week=0
        #weeks_per_year = 52
        if iri_year == 12:
            first_week = 1687 # for year 12
            last_week = 1739 # for year 12
        else:
            # TODO: support other years
            print("Only year 12 supported at the current time")
            return False

        df = self.runQueryOnRedshift(item, vend)
        if len(df.index) <= 0:
            print("No rows returned for the Redshift query using the specified (item,vend) pair in year-"+ str(iri_year))
            return False

        # file_path =self.filepath

        # if len(file_path) == 0 :
        #     print 'No path to the tsv file provided'
        #     return False

        # # extract path
        # dirname = os.path.dirname(file_path)
        # # extract file name without extension
        # filename = os.path.splitext(os.path.basename(file_path))[0]
        
        # # load data from tsv
        # df = pd.read_csv(file_path ,sep='\t', header=0)

        # get first iri_key from file
        df = df.sort_values(['item','vend', 'iri_key', 'week'], ascending=[True,True,True, True])
        #df.to_csv(file_path,sep='\t', header=True, index=False)
        #number_of_keys = len(df.groupby(['iri_key'])['dollars'].count())
        #bar = Bar('Processing', max_val=number_of_keys)


        # sweep through dataframe and 
        #df2 = df[df['iri_key']=='200156']
        #print(df2)
        #total_dollars = df.groupby(['iri_key'])['dollars'].count()
        
        # create a new empty dataframe to hold the promo table
        promo_table_colums = ['sample',
                              'item',
                              'vend',
                              'iri_key',
                              'promo_start_week', # we can compute (promo_end_week = promo_start_week + number_of_promo_weeks)
                              'promo_features',
                              'promo_display',
                              'avg_promo_unit_price', 
                              'number_of_promo_weeks', 
                              'number_of_datapoints', 
                              'total_units_sold_in_promo', 
                              'total_dollar_sales_value_in_promo', 
                              'number_of_weeks_in_pre_promo_window', # up to a max of 5
                              'number_of_weeks_in_post_promo_window', # up to a max of 5 
                              'total_units_sold_in_pre_promo_window',
                              'total_dollar_sales_in_pre_promo_window',
                              'total_units_sold_in_post_promo_window',
                              'total_dollar_sales_in_post_promo_window',
                              'promo_profit',
                              'promo_lift',
                              'md5hash']
                              
        df_promo = pd.DataFrame(columns=promo_table_colums)

        
        non_promo_table_colums = ['sample',
                                  'item',
                                  'vend',
                                  'iri_key', 
                                  'avg_weekly_units_sold_non_promo_weeks', 
                                  'avg_weekly_price_point_non_promo_weeks',
                                  'md5hash']
                                  
        df_non_promo = pd.DataFrame(columns=non_promo_table_colums)
        
        # use 5 weeks prior for window length
        window_pre = 5 #in weeks
        window_post = 5 # in weeks

        cur_total_non_promo_units_sold = 0
        cur_total_non_promo_dollar_sales = 0
        cur_total_non_promo_weeks = 0
        cur_total_non_promo_data_points = 0
        cur_non_promo_avg_price_point = 0

        last_iri_key = -1
        last_item = -1
        last_vend = -1
        promo_start_week = -1
        
        total_rows = len(df.index)

        nonPromoSample = 0
        print("Beginning promo analysis; will analyze a total of " + str(total_rows) + " rows")
        #----------------------------------------------------------------------
        for index, row in df.iterrows():
            
            cur_pr = int(row['pr'])
            cur_week = int(row['week'])
            cur = int(row['iri_key'])
            cur_item = int(row['item'])
            cur_vend= int(row['vend'])
            cur_units = int(row['units'])
            
            if last_iri_key != cur:
                #bar.next()
                # we have moved on to the next iri_key
                # rest variables
                print 'Analyzing group with iri_key = ', cur

                
                if last_iri_key != -1:
                    # update the non-promo week table
                    avg_weekly_non_promo_units_sold = 0
                    avg_non_promo_unit_price = 0
                    
                    if cur_total_non_promo_weeks != 0:
                        avg_weekly_non_promo_units_sold = round(cur_total_non_promo_units_sold/(cur_total_non_promo_weeks*1.0),2)
                    if cur_total_non_promo_data_points != 0:
                        avg_non_promo_unit_price = round(cur_non_promo_avg_price_point/(cur_total_non_promo_data_points*1.0),2)
                    ## print 'Updating non-promo table'
                    entryIndex = len(df_non_promo)
                    df_non_promo.loc[entryIndex]= [entryIndex-1, last_item, last_vend, last_iri_key, avg_weekly_non_promo_units_sold, avg_non_promo_unit_price, '']
                    # as a final step add the md5hash column to uniquely identify this row in the table based on its values
                    md5hashStr = self.generateMD5Hash(str(df_non_promo.ix[entryIndex].values))
                    df_non_promo.set_value(entryIndex, 'md5hash',md5hashStr) # use entryIndex since the length of the length of the data frame has increaased by one to the prior insert
                    

            
                prior_week_pr = 0
                cur_total_non_promo_units_sold = 0
                cur_total_non_promo_dollar_sales = 0
                cur_total_non_promo_weeks = 0
                cur_total_non_promo_data_points = 0
                cur_non_promo_avg_price_point = 0
                
                cur_total_promo_units_sold = 0
                cur_total_promo_dollar_sales = 0
                cur_total_promo_weeks = 0
                cur_total_promo_data_points = 0
                cur_promo_avg_price_point = 0
                cur_promo_features =[]
                cur_promo_display =[]
                
                # pre/post promo windows
                window_units = []
                window_sales =[]
                pre_promo_units_window = []
                pre_promo_sales_window = []
                pre_promo_window_length = 0
                total_sales_pre_promo = 0
                total_units_pre_promo = 0
                prior_week = first_week-1 # assume all weeks not present in current year are missing
            
            # update iri_key
            last_iri_key = cur

            last_item = cur_item
            last_vend = cur_vend
                
            # we are still analyzing the same iri_key
            missing_samples = cur_week - prior_week -1
            
            # update week
            prior_week = cur_week
            
            if cur_pr == 0:
                # a non-promo day
            
                # only update the window in non-promo weeks
                window_units =  window_units + [0]*missing_samples
                window_units.append(cur_units)
            
                window_sales =  window_sales + [0]*missing_samples
                window_sales.append(row['dollars'])      
            
                cur_total_non_promo_units_sold += cur_units
                cur_total_non_promo_dollar_sales += row['dollars']
                if cur_units != 0:
                    cur_non_promo_avg_price_point += round((row['dollars'] / (cur_units * 1.0 )),2)
                    
                cur_total_non_promo_weeks += (1 + missing_samples) # accounts for missing weeks
                cur_total_non_promo_data_points += 1 # ignores missing weeks
                
                if prior_week_pr == 1:
                    # promo -> non_promo; this signals the end of a past promotion
                    ##print('End of Promo; Adding entry to promo table')
                    # write an entry into the final table for this promotion
                    # note: -1 or 0 or -99999 entries will be updated at a subsequent time
                    avg_promo_unit_price = 0
                    
                    if cur_total_promo_data_points != 0:            
                        avg_promo_unit_price = round((cur_promo_avg_price_point/(cur_total_promo_data_points * 1.0)),2)
                    
                    df_promo.loc[len(df_promo)]= [len(df_promo)-1,
                                                  cur_item,
                                                  cur_vend,
                                                  cur,
                                                  promo_start_week,
                                                  str(list(OrderedDict.fromkeys(cur_promo_features))), #TODO: find a better way to store this
                                                  str(list(OrderedDict.fromkeys(cur_promo_display))), #TODO: find a better way to store this
                                                  avg_promo_unit_price, 
                                                  cur_total_promo_weeks, 
                                                  cur_total_promo_data_points, 
                                                  cur_total_promo_units_sold, 
                                                  cur_total_promo_dollar_sales, 
                                                  pre_promo_window_length,
                                                  0,
                                                  total_units_pre_promo,
                                                  total_sales_pre_promo,
                                                  0,
                                                  0,
                                                  -99999, # this is a 'NaN' like value indicating an invalid entry
                                                  -99999, # this is a 'NaN' like value indicating an invalid entry
                                                  '']
                    
                    # reset the length of the prepromo window
                    pre_promo_window_length =0
            else:
                # a promo week
                if prior_week_pr == 0:
                    # non-promo -> promo transition (reset all promo related parameters)
                    ##print('Start of Promo')
                    cur_total_promo_units_sold = 0
                    cur_total_promo_dollar_sales = 0
                    cur_total_promo_weeks = 0
                    cur_total_promo_data_points = 0
                    cur_promo_avg_price_point = 0
                    cur_promo_features =[]
                    cur_promo_display =[]
                    
                    promo_start_week = cur_week
                    window_units =  window_units + [0]*missing_samples
                    window_sales =  window_sales + [0]*missing_samples
                    # get the last N weeks of the pre-promo window
                    pre_promo_units_window = window_units[-window_pre:]
                    pre_promo_sales_window = window_sales[-window_pre:]
                    pre_promo_window_length = len(pre_promo_units_window)
                    
                    total_units_pre_promo = sum(pre_promo_units_window)
                    total_sales_pre_promo = sum(pre_promo_sales_window)
                    
                    
                    # this post promo corresponds to the prior promotion
                    # get the first N weeks of the post-promo window
                    post_promo_units_window = window_units[:window_post]  
                    post_promo_sales_window = window_sales[:window_post]
                    
                    
                    total_units_post_promo = sum(post_promo_units_window)
                    total_sales_post_promo = sum(post_promo_sales_window)
                    
                    # update the last promo entry corresponding to the current key (if any) with the post promo values
                    if len(df_promo) > 0:
                        # there is at least one entry in the promo table so far
                        last_promo_indx = len(df_promo)-1
                        if df_promo.loc[last_promo_indx]['iri_key'] == cur:
                            # this means there is a past promo in the table corresponding to this iri_key
                            # we need to update its post promo info here
                            ##print 'Backtracking - updating post-promo window of past promo'
        
                            df_promo.set_value(last_promo_indx, 'number_of_weeks_in_post_promo_window',len(post_promo_units_window))
                            df_promo.set_value(last_promo_indx,'total_units_sold_in_post_promo_window', total_units_post_promo)
                            df_promo.set_value(last_promo_indx,'total_dollar_sales_in_post_promo_window', total_sales_post_promo)
                        
                    
                    # reset the pre/post promo windows
                    window_units =[]
                    window_sales = []
                    pre_promo_units_window=[]
                    pre_promo_sales_window=[]
                    post_promo_units_window=[]
                    post_promo_sales_window=[]
        
                    
                cur_total_promo_units_sold += cur_units
                cur_total_promo_dollar_sales += row['dollars']
                cur_promo_features.append(row['f'])
                cur_promo_display.append(row['d'])
                if cur_units != 0:
                    cur_promo_avg_price_point += round((row['dollars'] / (cur_units * 1.0 )),2)
                if prior_week_pr == 1:
                    cur_total_promo_weeks += (1 + missing_samples) # accounts for missing weeks as if they wer promo weeks 1?1 -> ?=1
                else:
                    cur_total_promo_weeks += 1 # since we assume 0?1 then we assume ? = 0 (i.e. a non promo week)
                    cur_total_non_promo_weeks += missing_samples # accounts for missing weeks
                    
                cur_total_promo_data_points += 1 # ignores missing weeks
            
            # update the prior week pr vlaue
            prior_week_pr = row['pr']
        #----------------------------------------------------------------------           
        # update the non-promo week table (for the last entry)
        avg_weekly_non_promo_units_sold = -1
        avg_non_promo_unit_price = -1    
        
        if cur_total_non_promo_weeks != 0:
            avg_weekly_non_promo_units_sold = round(cur_total_non_promo_units_sold/(cur_total_non_promo_weeks*1.0),2)
        if cur_total_non_promo_data_points != 0:
            avg_non_promo_unit_price = round(cur_non_promo_avg_price_point/(cur_total_non_promo_data_points*1.0),2)
        
        entryIndex = len(df_non_promo)
        df_non_promo.loc[entryIndex]= [entryIndex-1, last_item, last_vend, last_iri_key, avg_weekly_non_promo_units_sold, avg_non_promo_unit_price, '']
        # as a final step add the md5hash column to uniquely identify this row in the table based on its values
        md5hashStr = self.generateMD5Hash(str(df_non_promo.ix[entryIndex].values))
        df_non_promo.set_value(entryIndex, 'md5hash',md5hashStr) # use entryIndex since the length of the length of the data frame has increaased by one to the prior insert
             
        #bar.finish()
        
        # now for each promo entry compute the price reduction by comparing the avg price on non-promo days to the avg price on promo days
        
        print("Computing promotion metrics (lift & profit)")
        # the promo's profit is the total money made during the promo minus the los due to the price rduction
        # profit_during_promo_period = total_sales_in_promo_period - (number_of_promo_weeks * (NON_promo_price - promo_price) )
        # lift1 = total_sales_during_promo/total_prom_weeks - total_sales_during_non_promo_pre/post_window/non_promo_pre/post_window
        # if lift1 is big => avg money made during promotional period brought in an increase in profit as opposed to avg money made on non promo day => successful promo!
        # lift2 = total_sales_post_promo_window  - total_sales_pre_window      
        for index, row in df_promo.iterrows():
             profit = 0
             avg_non_promo_unit_price = df_non_promo[df_non_promo['iri_key'] == row['iri_key']]['avg_weekly_price_point_non_promo_weeks']
                     
             if avg_non_promo_unit_price.empty == False:
                 profit = row['total_dollar_sales_value_in_promo'] - row['number_of_promo_weeks']* (avg_non_promo_unit_price-  row['avg_promo_unit_price'])
             df_promo.set_value(index, 'promo_profit',profit)
             lift = -1
             number_of_non_promo_weeks = row['number_of_weeks_in_pre_promo_window']+row['number_of_weeks_in_post_promo_window']
             if row['number_of_promo_weeks'] != 0 and number_of_non_promo_weeks != 0:
                 lift = row['total_dollar_sales_value_in_promo']/(1.0 *row['number_of_promo_weeks']) - (row['total_units_sold_in_pre_promo_window']+row['total_units_sold_in_post_promo_window'])/(1.0*number_of_non_promo_weeks)
             df_promo.set_value(index, 'promo_lift',lift)
             
             # as a final step add the md5hash column to uniquely identify this row in the table based on its values
             md5hashStr = self.generateMD5Hash(str(df_promo.ix[index].values))
             df_promo.set_value(index, 'md5hash',md5hashStr)
             
        
        print("Promo Analysis complete")
        print '-------------------------------'
        
        print("Number of promotions discovered: " + str(len(df_promo.index)))
        print("Number of iri_keys proccessed: " + str(len(df_non_promo.index)))
        print '-------------------------------'
         # save the resulting datframes to a tsv                         
        #df_promo.to_csv(dirname +'/'+ filename +'_promo_table.tsv',sep='\t', header=True, index=False)
        #df_non_promo.to_csv(dirname +'/'+ filename +'_non_promo_table.tsv',sep='\t', header=True, index=False)

        # save the results to redshift tables
        # TODO: dont overwrite tables; try to append to them or dont do anything
                
        filename = 'year' + str(iri_year)
        if item != None:
            filename += '_item' + str(item) 
        
        if vend != None:
            filename += '_vend' + str(vend) 

        self.df_promo = df_promo
        self.df_non_promo = df_non_promo
        #save2Db(df, filename)
        
        promoTypeDict = {'sample' : types.Integer, \
            'item' : types.Integer, \
            'vend' : types.Integer, \
            'iri_key' : types.Integer, \
            'promo_start_week' : types.Integer, \
            'promo_features' : types.Text, \
            'promo_display' : types.Text, \
            'avg_promo_unit_price' : types.Numeric, \
            'number_of_promo_weeks' : types.Integer, \
            'number_of_datapoints' : types.Integer, \
            'total_units_sold_in_promo' : types.Integer, \
            'total_dollar_sales_value_in_promo' : types.Numeric, \
            'number_of_weeks_in_pre_promo_window' : types.Integer, \
            'number_of_weeks_in_post_promo_window' : types.Integer, \
            'total_units_sold_in_pre_promo_window' : types.Integer, \
            'total_dollar_sales_in_pre_promo_window' : types.Numeric, \
            'total_units_sold_in_post_promo_window' : types.Integer, \
            'total_dollar_sales_in_post_promo_window' : types.Numeric, \
            'promo_profit' : types.Numeric, \
            'promo_lift' : types.Numeric, \
            'md5hash' : types.Text} # note: in Redshift the UNIQUE constraint is only informational and not enforced by the system -> the md5hash column wont prevent duplicates but may simplify finding them

        nonPromoTypeDict = {'sample' : types.Integer, \
            'item' : types.Integer, \
            'vend' : types.Integer, \
            'iri_key' : types.Integer, \
            'avg_weekly_units_sold_non_promo_weeks' : types.Integer, \
            'avg_weekly_price_point_non_promo_weeks' : types.Numeric, \
            'md5hash' : types.Text}
            
        self.save2Db(df = df_promo, tableName = 'promo_' + filename, columnTypes = promoTypeDict)
        self.save2Db(df = df_non_promo, tableName = 'non_promo_' + filename, columnTypes = nonPromoTypeDict)
        
        return True

    def chunks(self,l, n):
        n = max(1, n)
        return [l[i:i + n] for i in range(0, len(l), n)]
    
    # TODO: make this a more generic interface with ability to save to other databss than mysql (like C*, Redshift, etc..)
    def save2Db(self, df,tableName, columnTypes):
        
        host=self.__HOST
        port=self.__PORT
        user=self.__USER
        password=self.__PASSWORD
        database=self.__DATABASE
        engine_string = "postgresql+psycopg2://%s:%s@%s:%d/%s" \
        % (user, password, host, port, database)
        engine = create_engine(engine_string)
        conn = engine.connect()
        #metadata = MetaData(conn)
        #test = pd.read_sql_query(test_query,red_engine)
        #engine = create_engine('mysql+mysqlconnector://gauss:gaussanalytics123@[host]:3306/[schema]', echo=False)
        try:
            print("Writing dataframe <"+ tableName + "> to Redshift")
            #df.to_sql('data_chunked', engine, chunksize=1000)
            chunkSize=100
            #creatTableSqlPromo = "create table if not exists promo_year12_item41351_vend37000 \
            chunkArray = self.chunks(range(0,len(df.index)-1),chunkSize)
            k=0
            for i in chunkArray:
                k += 1
                df.ix[i].to_sql(name=tableName, con=conn, if_exists = 'append', index=False, chunksize=100, dtype= columnTypes)
                print("writing chunk #" + str(k) + "/" + str(len(chunkArray)) + " to Redshift")
         
            #df.to_csv('tmp/queryresult.csv', index=False)
        finally:
            print("Closing connection to Redshift")
            conn.close()        
        


