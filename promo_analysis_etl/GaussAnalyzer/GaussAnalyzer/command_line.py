## command_line.py

# Modify this file to allow Python functions (not scripts!) to be directly registered as command-line accessible tools.
# useage:
# 	python
# 	import gauss_iri.command_line
# 	gauss_iri.command_line.main()
from . import GaussAnalyzer

def main():
    # main function
	#if __name__ == '__main__':
    import pandas as pd
    #import matplotlib.pyplot as plt
    import sys
    # uncomment below line to do the etl
    if len(sys.argv) != 2:
        # no arg provided -> throw error
        print 'No path to the tsv file provided'
        sys.exit()
    
    # ex. /Users/oliversaleh/work/Gauss/toothpaste_year12_item41351_vend37000.tsv
    file_path = str(sys.argv[1])
    ga = gauss_iri()
    pr = ga.PromoAnalyzer(file_path)
    pr.promoAnalyticsEtl()
    #df_promo = pd.read_csv('/Users/oliversaleh/work/Gauss/toothpaste_year12_item41351_vend37000_promo_table.tsv',sep='\t', header=0)
    #plt.close("all")
    #plt.bar(df_promo.index.values, df_promo['promo_lift'].values)
    #plt.show() # Depending on whether you use IPython or interactive mode, etc.
