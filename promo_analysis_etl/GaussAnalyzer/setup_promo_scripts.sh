## on Host machine

# TODO: change this
#CONTAINER_NAME=condescending_shockley
TSV_TABLE_NAME=toothpaste_year12_item41351_vend37000.tsv

# docker container must be running
#sh ./check_docker_container.sh gaussian_brainiac


docker start $CONTAINER_NAME
# copy the tables from local host machine to the container dev/tsv directory
docker cp TSV_TABLE_NAME $CONTAINER_NAME:/dev/tsv/toothpaste_year12_item41351_vend37000.tsv


## On docker container
# attach to the docker image (asume one already exists called gaussian_brainiac and based on image anaconda:latest)
docker attach $CONTAINER_NAME
#----------------------
# in container>>>
# Todo: do i need to repeat this?
CONTAINER_NAME=condescending_shockley
TSV_TABLE_NAME=toothpaste_year12_item41351_vend37000.tsv


# get the python code from bitbucket
mkdir -p /dev/tsv

REPOSRC=https://oliver-gauss-io@bitbucket.org/gaussanalytics/spark-etl-iri.git
LOCALREPO=/dev/promo_etl

# We do it this way so that we can abstract if from just git later on
LOCALREPO_VC_DIR=$LOCALREPO/.git

if [ ! -d $LOCALREPO_VC_DIR ]
then
    git clone $REPOSRC $LOCALREPO
    cd $LOCALREPO
else
    cd $LOCALREPO
    git pull $REPOSRC
fi

git checkout oliver/python_etl_promo_analysis_scripts

# invoke the python code to generate the tables
cd spark-etl-iri/promo_analysis_etl
python PreparePromoAnalysisTrainingTables.py /dev/tsv/$TSV_TABLE_NAME

