#!/bin/sh

# TODO: Add try-catch clause
# TODO: Add script to install neccessary depndencies below if not already setup locally

# Dependencies -----------
# node -v
# -> v6.3.1
# npm -v
# -> 3.10.3
# pip --version
# -> pip 8.1.2 (python 3.5)
# python 3.5 env
# --------------------------

# get the analyzer directory (even if accessed from a different dir or a symlink)
# see here: https://stackoverflow.com/questions/1371261/get-current-directory-name-without-full-path-in-bash-script
#SOURCE="${BASH_SOURCE[0]}"
SOURCE=$0
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
GAUSSANALYZER_HOME_DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
#echo $GAUSSANALYZER_HOME_DIR
CURRENT_DIR=$PWD
#echo $CURRENT_DIR
cd $GAUSSANALYZER_HOME_DIR

#switch to python 3.5 env
# source activate python3_dev

# open new cmd window
cd $GAUSSANALYZER_HOME_DIR
# delete dist directory if it exists
rm -rf dist
# build python package

# install dependencies for psycopg2
apt-get install gcc
apt-get install libpq-dev python-dev
#pip uninstall psycopg2
#pip list --outdated
pip install --upgrade wheel
pip install --upgrade setuptools
pip install psycopg2

python setup.py sdist
# this will create a dist dir containing a tar.gz package
# ===========================
# open new window and start http server
mkdir dist
cd dist
# start http server on port 9000 bound to local host (in background)
pip install http
# python -m http.server 9000 --bind 127.0.0.1 &  # for python 3.5
python -m SimpleHTTPServer 9000 --bind 127.0.0.1 & # for python 2.7
# ===========================
# go back to original window
pip install  http://127.0.0.1:9000/GaussAnalyzer-0.0.1.tar.gz
# kill the server started earlier
#ps aux | grep "python -m http.server" | awk '{print $2}' | xargs kill -9  # for python 3.5
ps aux | grep "python -m SimpleHTTPServer" | awk '{print $2}' | xargs kill -9  # for python 2.7

# switch away from python 3.5 env
# source deactivate

cd $CURRENT_DIR