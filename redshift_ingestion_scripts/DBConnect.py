from sqlalchemy import create_engine
import pandas
import psycopg2
import os
import logging

logger = logging.getLogger('root')
FORMAT = "[%(lineno)s - %(funcName)s - %(levelname)s] %(message)s"
logging.basicConfig(format=FORMAT)
logger.setLevel(logging.DEBUG)

def read_env_vars():
    logger.info("Read Environment variables")
    global HOST
    global PORT
    global DBNAME
    global USER
    global PASSWORD
    HOST     = os.getenv("REDSHIFT_IP_ADDRESS", "localhost")
    PORT     = os.getenv("DB_PORT", "5439")
    DBNAME   = os.getenv("DB_NAME", "")
    USER     = os.getenv("DB_USER", "")
    PASSWORD = os.getenv("REDSHIFT_DW_PASSWORD", "")
	
def acquireTableFromDevHackToCSV(PythonCodePath, ShemaTableName, packageName, DBauthentication):
	engine = create_engine(DBauthentication)
	c = engine.connect()
	if (packageName != ""): 
		query = 'select * from '+ShemaTableName+' where '+ShemaTableName+'.package_name = '+ "'" + packageName + "'"
	else:
		query = 'select * from '+ShemaTableName
	df = pandas.read_sql_query(query,con=c)
	c.close()
	PythonCodePath = PythonCodePath+'/AquiredTable.tsv'
	df.to_csv(PythonCodePath, sep='\t', encoding= "utf8")

def acquireNewUnlabeledDataFromDevHackToCSV(PythonCodePath, packageName, DBauthentication):
	engine = create_engine(DBauthentication)
	c = engine.connect()
	query1 = 'SELECT * FROM gplay.view_gplay_translated_reviews WHERE has_review = 1 AND package_name = ' + " '" + packageName + "' " + ' AND category_name IS NULL '  	
	df1 = pandas.read_sql_query(query1, con=c )
	label = "Needs Human Intervention"
	query2 = 'select A.* from gplay.view_gplay_translated_reviews as A where A.package_name = ' + " '" + packageName + "' " + 'AND A.has_review=1 AND A.category_name = '+" '"+label+ "'"  	
	df2 = pandas.read_sql_query(query2, con=c )
	c.close()
	df = df1.append(df2)
	PythonCodePath = PythonCodePath+'/NewTestData.tsv'
	df.to_csv(PythonCodePath, sep='\t', encoding = 'utf8')
	
def RowBindTable(PythonCodePath,ShemaTableName, csvFileName, DBauthentication, packageName):
	df= pandas.read_csv(PythonCodePath+'/'+csvFileName, sep='\t', encoding= "utf8")	
	num_rows = df.shape[0]
	read_env_vars()
	conn_url = "host=" + HOST + " port=" + PORT + " dbname=" + DBNAME + " user=" + USER + " password=" + PASSWORD + " sslmode=require"
	conn = psycopg2.connect(conn_url)
	cur =  conn.cursor()
	records_list_template = ",".join(['%s'] * num_rows) 
	insert_query = "INSERT INTO " + ShemaTableName + " (link, package_name, review_updated_datetime, review_text_translated, category_name, secondary_category_name, category_updated_datetime, is_manual_category, star_rating) VALUES {0}".format(records_list_template)
	reviews_labels = [tuple(row) for row in df[['link', 'package_name', 'review_updated_datetime', 'review_text_translated', 'category_name', 'secondary_category_name', 'category_updated_datetime', 'is_manual_category', 'star_rating']].values.tolist()]
	label= 'Needs Human Intervention'
	deleteQuery= "delete from gplay.review_analysis where category_name = "+"'"+label+"' " + "AND package_name =" + " '" + packageName + "' "
	cur.execute(deleteQuery)
	cur.execute(insert_query, reviews_labels)
	conn.commit()
	cur.close()
	conn.close()
	
	
	#engine = create_engine(DBauthentication)
	#df.to_sql(ShemaTableName, engine, if_exists='append' , index=False , index_label=None)

def acquireAllCategoryLabelsFromDevHackToCSV(PythonCodePath, DBauthentication):
	engine = create_engine(DBauthentication)
	c = engine.connect()
	query = 'select Distinct A.category_name from gplay.review_analysis as A where A.category_name is NOT NULL'
	df = pandas.read_sql_query(query,con=c)
	c.close()
	PythonCodePath = PythonCodePath+'/AquiredTable.tsv'
	df.to_csv(PythonCodePath, sep='\t' , encoding = 'utf8')
	
def readOverallTableFromDevHack(PythonCodePath, DBauthentication):
	query = 'select A.* from gplay.overall as A where A.date >='+"'"+'2016-03-01'+"'"+'and A.package_name = '+"'"+'com.samsung.android.spay'+"'"
	engine = create_engine(DBauthentication)
	c = engine.connect()
	df = pandas.read_sql_query(query,con=c)
	c.close()
	PythonCodePath = PythonCodePath+'/AquiredTable.tsv'
	df.to_csv(PythonCodePath, sep='\t' , encoding = 'utf8')
	
def readReviewsTableFromDevHack(PythonCodePath, DBauthentication):
	query = 'select A.* from gplay.view_gplay_translated_reviews as A where A.updated_date >='+"'"+'2016-03-01'+"'"+'and A.package_name = '+"'"+'com.samsung.android.spay'+"'"
	engine = create_engine(DBauthentication)
	c = engine.connect()
	df = pandas.read_sql_query(query,con=c)
	c.close()
	PythonCodePath = PythonCodePath+'/AquiredTable.tsv'
	df.to_csv(PythonCodePath, sep='\t' , encoding = 'utf8')
	
def readSamsungPayTransactionTables(PythonCodePath, FromDateTime, ToDateTime, DBauthentication):  
	query1 = 'select * from samsung_pay.enrollments where ts > '+FromDateTime+ 'AND ts < '+ ToDateTime
	query2 = 'select * from samsung_pay.receipts where txn_ts > '+FromDateTime+ 'AND txn_ts < '+ ToDateTime
	query3 = 'select * from samsung_pay.reports where ts > '+FromDateTime+ 'AND ts < '+ ToDateTime
	query4 = 'select * from samsung_pay.token_data where first_seen_ts > '+FromDateTime+ 'AND first_seen_ts < '+ ToDateTime
	query5 = 'select * from samsung_pay.tokens where ts > '+FromDateTime+ 'AND ts < '+ ToDateTime
	query6 = 'select * from samsung_pay.txns where ts > '+FromDateTime+ 'AND ts < '+ ToDateTime
	engine = create_engine(DBauthentication)
	c = engine.connect()
	df = pandas.read_sql_query(query1,con=c)
	
	PythonCodePath = PythonCodePath+'/AquiredTable.tsv'
	df.to_csv(PythonCodePath, sep='\t' , encoding = 'utf8')
	
	c.close()
	
def ReadGenericTableFromDevHack(query, PythonCodePath, DBauthentication): 
	engine = create_engine(DBauthentication)
	c = engine.connect()
	df = pandas.read_sql_query(query,con=c)
	PythonCodePath = PythonCodePath+'/AquiredTable.tsv'
	df.to_csv(PythonCodePath, sep='\t' , encoding = 'utf8')

def RowBindIRISchema(PythonCodePath,ShemaTableName, csvFileName, DBauthentication):
    df= pandas.read_csv(PythonCodePath+'/'+csvFileName, sep='\t', encoding= "utf8")	
    num_rows = df.shape[0]
    read_env_vars()
    conn_url = DBauthentication
    conn = psycopg2.connect(conn_url)
    cur =  conn.cursor()
    records_list_template = ",".join(['%s'] * num_rows)
    if(ShemaTableName == "years.panel"):
        insert_query = "INSERT INTO " + ShemaTableName + " (PANID, WEEK, UNITS, OUTLET, DOLLARS, IRI_KEY, COLUPC) VALUES {0}".format(records_list_template)
        reviews_labels = [tuple(row) for row in df[['PANID', 'WEEK', 'UNITS', 'OUTLET', 'DOLLARS', 'IRI_KEY', 'COLUPC']].values.tolist()]
    else:
        insert_query = "INSERT INTO " + ShemaTableName + " (IRI_KEY, WEEK, SY, GE, VEND, ITEM, UNITS, DOLLARS, F, D, PR) VALUES {0}".format(records_list_template)
        reviews_labels = [tuple(row) for row in df[['IRI_KEY', 'WEEK', 'SY', 'GE', 'VEND', 'ITEM', 'UNITS', 'DOLLARS', 'F', 'D', 'PR']].values.tolist()]
    
    cur.execute(insert_query, reviews_labels)
    conn.commit()
    cur.close()
    conn.close()
    
    
def RowBindIRISchemaToothPaste(PythonCodePath,ShemaTableName, csvFileName, DBauthentication):
    df= pandas.read_csv(PythonCodePath+'/'+csvFileName, sep='\t', encoding= "utf8")	
    num_rows = df.shape[0]
    read_env_vars()
    conn_url = DBauthentication
    conn = psycopg2.connect(conn_url)
    cur =  conn.cursor()
    records_list_template = ",".join(['%s'] * num_rows)
    
    insert_query = "INSERT INTO " + ShemaTableName + " (L1, L2, L3, L4, L5, L9, Level, UPC, SY, GE, VEND, ITEM, STUBSPEC, VOL_EQ, PRODUCT_TYPE, FLAVOR_SCENT, TYPE_OF_FORMULA, FORM, USER_INFO, ADDITIVES, PACKAGE) VALUES {0}".format(records_list_template)
    reviews_labels = [tuple(row) for row in df[['L1', 'L2', 'L3', 'L4', 'L5', 'L9', 'Level', 'UPC', 'SY', 'GE', 'VEND', 'ITEM', 'STUBSPEC', 'VOL_EQ', 'PRODUCT_TYPE', 'FLAVOR_SCENT', 'TYPE_OF_FORMULA', 'FORM', 'USER_INFO', 'ADDITIVES', 'PACKAGE']].values.tolist()]
    
    cur.execute(insert_query, reviews_labels)
    conn.commit()
    cur.close()
    conn.close()

def RowBindIRISchemaDeliveryStore(PythonCodePath,ShemaTableName, csvFileName, DBauthentication):
    df= pandas.read_csv(PythonCodePath+'/'+csvFileName, sep='\t', encoding= "utf8")	
    num_rows = df.shape[0]
    read_env_vars()
    conn_url = DBauthentication
    conn = psycopg2.connect(conn_url)
    cur =  conn.cursor()
    records_list_template = ",".join(['%s'] * num_rows)
    
    insert_query = "INSERT INTO " + ShemaTableName + " (IRI_KEY, OU, EST_ACV, MARKET_NAME, OPN, CLSD, MSKDNAME) VALUES {0}".format(records_list_template)
    reviews_labels = [tuple(row) for row in df[['IRI_KEY', 'OU', 'EST_ACV', 'MARKET_NAME', 'OPN', 'CLSD', 'MSKDNAME' ]].values.tolist()]
    cur.execute(insert_query, reviews_labels)
    conn.commit()
    cur.close()
    conn.close()