dataSourcePath = 'G:/SchemaTransformation/dataSource'
## extract all the paths of each file in folders and subfolders
import os
with open("output.txt", "w") as a:
    for path, subdirs, files in os.walk(dataSourcePath):
        for filename in files:
            f = os.path.join(path, filename)
            a.write(str(f) + os.linesep)  
             
    filenames = [line.rstrip('\n') for line in open('output.txt')]
    filenames= [x for x in filenames if x]