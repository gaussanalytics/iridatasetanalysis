## function to upload folders into S3
def uploadFolder(p, AWS_KEY, AWS_SECRET):
    import os
    from boto.s3.connection import S3Connection
    import threading
    ## extract all the paths of each file in folders and subfolders
    with open("output.txt", "w") as a:
        for path, subdirs, files in os.walk(p):
           for filename in files:
             f = os.path.join(path, filename)
             a.write(str(f) + os.linesep)  
             
    filenames = [line.rstrip('\n') for line in open('output.txt')]
    filenames= [x for x in filenames if x]

    AWS_KEY = os.getenv('AWS_KEY') 
    AWS_SECRET = os.getenv('AWS_SECRET')
    
    ## Multi-threading upload
    for fname in filenames:
        t = threading.Thread(target = upload, args=(fname,)).start() 

def upload(myfile):
    conn = S3Connection(aws_access_key_id=AWS_KEY, aws_secret_access_key=AWS_SECRET)
    bucket = conn.get_bucket("gaussanalytics")
    key = bucket.new_key(myfile).set_contents_from_string(open(myfile).read()) ## doesnt work with encrypted files such as pdfs 
    return myfile
 
    
########## Function to upload a file any type of file to S3
def uploadfile(path, AWS_KEY, AWS_SECRET):
    import math, os
    import boto
    from filechunkio import FileChunkIO

    # Connect to S3
    c = boto.connect_s3(aws_access_key_id = AWS_KEY, aws_secret_access_key = AWS_SECRET)
    b = c.get_bucket('gaussanalytics')

    # Get file info
    source_path = 'G:/Academic Dataset External.zip'
    source_size = os.stat(source_path).st_size

    # Create a multipart upload request
    mp = b.initiate_multipart_upload(os.path.basename(source_path))

    # Use a chunk size of 50 MiB (feel free to change this)
    chunk_size = 52428800
    chunk_count = int(math.ceil(source_size / float(chunk_size)))

    # Send the file parts, using FileChunkIO to create a file-like object
    # that points to a certain byte range within the original file. We
    # set bytes to never exceed the original file size.
    for i in range(chunk_count):
        offset = chunk_size * i
        bytes = min(chunk_size, source_size - offset)
        with FileChunkIO(source_path, 'r', offset=offset,bytes=bytes) as fp:
            mp.upload_part_from_file(fp, part_num=i + 1)

    # Finish the upload
    mp.complete_upload()